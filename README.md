# orch

This is a project to research and implement a method for automatically imitate a given sound with a sample orchestration.

## Idea

- Get a latent representation of an input sound in the form of feature encodings with wavenet(?)
- Find the "most similar representation" to the input sound. This representation is ideally a sum of different samples.
- Return the corresponding annotated information for the representation found.

## Useful links
https://medium.com/@LeonFedden/comparative-audio-analysis-with-wavenet-mfccs-umap-t-sne-and-pca-cb8237bfce2f
https://stefanofasciani.com/?p=1216

## Related newer
[orchidea][0]

### Related but older
http://www.thomashummel.net/images/publications/simulation_of_human_voice_timbre.pdf
Orchidee -> https://www.tandfonline.com/doi/abs/10.1080/09298210903581566?mobileUi=0&journalCode=nnmr20
Orchidee -> http://articles.ircam.fr/textes/Nouno09a/index.pdf
Orchidee -> https://muse.jhu.edu/article/483749/pdf

## Extra
https://cycling74.com/tools/digital-orchestra-toolbox


[0]: http://www.carminecella.com/orchidea/